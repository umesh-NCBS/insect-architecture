\chapter{Introduction}

It can never be too strongly impressed upon a mind anxious
for the acquisition of knowledge, that the commonest
things by which we are surrounded are deserving of minute
and careful attention. The most profound investigations of
Philosophy are necessarily connected with the ordinary
circumstances of our being, and of the world in which our
every-day life is spent. With regard to our own existence,
the pulsation of the heart, the act of respiration, the voluntary
movement of our limbs, the condition of sleep, are
among the most ordinary operations of our nature; and yet
how long were the wisest of men struggling with dark and
bewildering speculations before they could offer anything
like a satisfactory solution of these phenomena, and how far
are we still from an accurate and complete knowledge of
them! The science of Meteorology, which attempts to
explain to us the philosophy of matters constantly before our
eyes, as dew, mist, and rain, is dependent for its illustrations
upon a knowledge of the most complicated facts, such as the
influence of heat and electricity upon the air; and this
knowledge is at present so imperfect, that even these common
occurrences of the weather, which men have been observing
and reasoning upon for ages, are by no means satisfactorily
explained, or reduced to the precision that every science
should aspire to. Yet, however difficult it may be entirely
to comprehend the phenomena we daily witness, everything
in nature is full of instruction. Thus the humblest flower
of the field, although, to one whose curiosity has not been
excited, and whose understanding has, therefore, remained
uninformed, it may appear worthless and contemptible, is
valuable to the botanist, not only with regard to its place
in the arrangement of this portion of the Creator's works,
but as it leads his mind forward to the consideration
of those beautiful provisions for the support of vegetable
life, which it is the part of the physiologist to study and to
admire.

This train of reasoning is peculiarly applicable to the
economy of insects. They constitute a very large and
interesting part of the animal kingdom. They are everywhere
about us. The spider weaves his curious web in our
houses; the caterpillar constructs his silken cell in our
gardens; the wasp that hovers over our food has a nest not
far removed from us, which she has assisted to build with the
nicest art; the beetle that crawls across our path is also an
ingenious and laborious mechanic, and has some curious
instincts to exhibit to those who will feel an interest in
watching his movements; and the moth that eats into our
clothes has something to plead for our pity, for he came,
like us, naked into the world, and he has destroyed our
garments, not in malice or wantonness, but that he may
clothe himself with the same wool which we have stripped
from the sheep. An observation of the habits of these little
creatures is full of valuable lessons, which the abundance of
the examples has no tendency to diminish. The more such
observations are multiplied, the more are we led forward to
the freshest and the most delightful parts of knowledge; the
more do we learn to estimate rightly the extraordinary provisions
and most abundant resources of a creative Providence;
and the better do we appreciate our own relations
with all the infinite varieties of nature, and our dependence,
in common with the ephemeron that flutters its little hour
in the summer sun, upon that Being in whose scheme of
existence the humblest as well as the highest creature has its
destined purposes. ``If you speak of a stone,'' says St. Basil,
one of the Fathers of the Church, ``if you speak of a fly, a
gnat, or a bee, your conversation will be a sort of demonstration
of His power whose hand formed them, for the wisdom
of the workman is commonly perceived in that which is of
little size. He who has stretched out the heavens, and
dug up the bottom of the sea, is also He who has pierced a
passage through the sting of the bee for the ejection of its
poison.''

If it be granted that making discoveries is one of the most
satisfactory of human pleasures, then we may without hesitation
affirm, that the study of insects is one of the most
delightful branches of natural history, for it affords peculiar
facilities for its pursuit. These facilities are found in the
almost inexhaustible variety which insects present to the
curious observer. As a proof of the extraordinary number of
insects within a limited field of observation, Mr. Stephens
informs us, that in the short space of forty days, between the
middle of June and the beginning of August, he found, in
the vicinity of Ripley, specimens of above two thousand four
hundred species of insects, exclusive of caterpillars and grubs,---a
number amounting to nearly a fourth of the insects
ascertained to be indigenous. He further tells us, that,
among these specimens, although the ground had, in former
seasons, been frequently explored, there were about one
hundred species altogether new, and not before in any
collection which he had inspected, including several new
genera; while many insects reputed scarce were in considerable
plenty.\footnote{Stephen's Illustrations, vol. i., p. 72, note.}
The localities of insects are, to a certain
extent, constantly changing; and thus the study of them has,
in this circumstance, as well as in their manifold abundance,
a source of perpetual variety. Insects, also, which are
plentiful one year, frequently become scarce, or disappear
altogether, the next---a fact strikingly illustrated by the
uncommon abundance, in 1826 and 1827, of the seven-spot
lady-bird (\textit{Coccinella septempunctata}), in the vicinity of London,
though during the two succeeding summers this insect was
comparatively scarce, while the small two-spot lady-bird
(\textit{Coccinella bipunctata}) was plentiful.

There is, perhaps, no situation in which the lover of
nature and the observer of animal life may not find opportunities
for increasing his store of facts. It is told of a
state prisoner, under a cruel and rigorous despotism, that
when he was excluded from all commerce with mankind, and
was shut out from books, he took an interest and found
consolation in the visits of a spider; and there is no
improbability in the story. The operations of that persecuted
creature are among the most extraordinary exhibitions
of mechanical ingenuity; and a daily watching of the
workings of its instinct would beget admiration in a rightly-constituted
mind. The poor prisoner had abundant leisure
for the speculations in which the spider's web would enchain
his understanding. We have all of us, at one period or
other of our lives, been struck with some singular evidence
of contrivance in the economy of insects, which we have
seen with our own eyes. Want of leisure, and probably
want of knowledge, have prevented us from following up the
curiosity which for a moment was excited. And yet some
such accident has made men naturalists, in the highest
meaning of the term. Bonnet, evidently speaking of himself,
says, ``I knew a naturalist, who, when he was seventeen years
of age, having heard of the operations of the ant-lion, began
by doubting them. He had no rest till he had examined into
them: and he verified them, he admired them, he discovered
new facts, and soon became the disciple and the friend of the
Pliny of France\footnote{Contemplation de la Nature, part ii. ch. 42.}''
(Réaumur). It is not the happy fortune
of many to be able to devote themselves exclusively to the
study of nature, unquestionably the most fascinating of
human employments; but almost every one may acquire
sufficient knowledge to be able to derive a high gratification
from beholding the more common operations of animal life.
His materials for contemplation are always before him.
Some weeks ago we made an excursion to West Wood, near
Shooter's Hill, expressly for the purpose of observing the
insects we might meet with in the wood: but we had not
got far among the bushes, when heavy rain came on. We
immediately sought shelter among the boughs of some thick
underwood, composed of oak, birch, and aspen; but we
could not meet with a single insect, not even a gnat or a fly,
sheltered under the leaves. Upon looking more narrowly,
however, into the bushes which protected us, we soon found
a variety of interesting objects of study. The oak abounded
in galls, several of them quite new to us; while the leaves
of the birch and the aspen exhibited the curious serpentine
paths of the minute mining caterpillars. When we had
exhausted the narrow field of observation immediately around
us, we found that we could considerably extend it, by breaking
a few of the taller branches near us, and then examining
their leaves at leisure. In this manner two hours glided
quickly and pleasantly away, by which time the rain had
nearly ceased; and though we had been disappointed in our
wish to ramble through the wood, we did not return without
adding a few interesting facts to our previous knowledge of
insect economy.\footnote{The original observations in this volume 
which are marked by the initials J. R., are by J. Rennie, A.M., 
A.L.S., and those which are enclosed in brackets are by the 
Rev. J. G. Wood, M.A., F.L.S.}

It will appear, then, from the preceding observations,
that cabinets and collections, though undoubtedly of the
highest use, are by no means indispensable, as the observer
of nature may find inexhaustible subjects of study in every
garden and in every hedge. Nature has been profuse
enough in affording us materials for observation, when we
are prepared to look about us with that keenness of inquiry,
which curiosity, the first step in the pursuit of knowledge,
will unquestionably give. Nor shall we be disappointed
in the gratification which is thus within our reach. Were
it no more, indeed, than a source of agreeable amusement,
the study of insects comes strongly recommended to the
notice of the well-educated. The pleasures of childhood
are generally supposed to be more exquisite, and to contain
less alloy, than those of riper years; and if so, it must be
because then everything appears new and dressed in fresh
beauties: while in manhood, and old age, whatever has
frequently recurred begins to wear the tarnish of decay.
The study of nature affords us a succession of ``ever-new
delights,'' such as charmed us in childhood, when everything
had the attractions of novelty and beauty; and thus
the mind of the naturalist may have its own fresh and
vigorous thoughts, even while the infirmities of age weigh
down the body.

It has been objected to the study of insects, as well as
to that of Natural History in general, that it tends to withdraw
the mind from subjects of higher moment; that it
cramps and narrows the range of thought; and that it
destroys, or at least weakens, the finer creations of the
fancy. Now, we should allow this objection in its fullest
extent, and even be disposed to carry it further than is
usually done, if the collecting of specimens only, or, as the
French expressly call them, chips (\textit{échantillons}), be called a
study. But the mere collector is not, and cannot be, justly
considered as a naturalist; and, taking the term naturalist
in its enlarged sense, we can adduce some distinguished
instances in opposition to the objection. Rousseau, for
example, was passionately fond of the Linnaean botany,
even to the driest minutiae of its technicalities; and yet it
does not appear to have cramped his mind, or impoverished
his imagination. If Rousseau, however, be objected to as
an eccentric being, from whose pursuits no fair inference
can be drawn, we give the illustrious example of Charles
James Fox, and may add the names of our distinguished
poets, Goldsmith, Thomson, Gray, and Darwin, who were
all enthusiastic naturalists. We wish particularly to insist
upon the example of Gray, because he was very partial
to the study of insects. It may be new to many of our
readers, who are familiar with the `Elegy in a Country
Churchyard,' to be told that its author was at the pains to
turn the characteristics of the Linnaean orders of insects
into Latin hexameters, the manuscript of which is still
preserved in his interleaved copy of the `Systema Naturae.'
Further, to use the somewhat exaggerated words of Kirby
and Spence, whose work on Entomology is one of the most
instructive and pleasing books on the science, ``Aristotle
among the Greeks, and Pliny the Elder among the Romans,
may be denominated the fathers of Natural History, as well
as the greatest philosophers of their day; yet both these
made insects a principal object of their attention: and in
more recent times, if we look abroad, what names greater
than those of Redi, Malpighi, Vallisnieri, Swammerdam,
Leeuwenhoek, Réaumur, Linnaeus, De Geer, Bonnet, and
the Hubers? and at home, what philosophers have done
more honour to their country and to human nature than
Ray, Willughby, Lister, and Derham? Yet all these made
the study of insects one of their most favourite
pursuits.''\footnote{Introduction to Entomology, vol. i.}

And yet this study has been considered, by those who
have superficially examined the subject, as belonging to a
small order of minds; and the satire of Pope has been
indiscriminately applied to all collectors, while, in truth, it
only touches those who mistake the means of knowledge
for the end:---

\begin{quotation}
``O! would the sons of men once think their eyes\\
And reason given them but to study Flies!\\
See Nature, in some partial, narrow shape,\\
And let the Author of the whole escape;\\
Learn but to trifle; or, who most observe,\\
To wonder at their Maker, not to serve.''\sourceatright{Dunciad, book iv.}
\end{quotation}

Thus exclaims the Goddess of Dulness, sweeping into her
net all those who study nature in detail. But if the matter
were rightly appreciated, it would be evident that no part
of the works of the Creator can be without the deepest
interest to an inquiring mind; and that a portion of creation
which exhibits such extraordinary manifestations of design
as is shown by insects must have attractions for the very
highest understanding.

An accurate knowledge of the properties of insects is
of great importance to man, merely with relation to his
own comfort and security. The injuries which they inflict
upon us are extensive and complicated; and the remedies
which we attempt, by the destruction of those creatures,
both insects, birds, and quadrupeds, who keep the ravages
in check, are generally aggravations of the evil, because
they are directed by an ignorance of the economy of nature.
The little knowledge which we have of the modes by which
insects may be impeded in their destruction of much that
is valuable to us, has probably proceeded from our contempt
of their individual insignificance. The security of property
has ceased to be endangered by quadrupeds of prey, and
yet our gardens are ravaged by aphides and caterpillars.
It is somewhat startling to affirm that the condition of the
human race is seriously injured by these petty annoyances;
but it is perfectly true that the art and industry of man
have not yet been able to overcome the collective force, the
individual perseverance, and the complicated machinery of
destruction which insects employ. A small ant, according
to a most careful and philosophical observer, opposes almost
invincible obstacles to the progress of civilization in many
parts of the equinoctial zone. These animals devour paper
and parchment; they destroy every book and manuscript.
Many provinces of Spanish America cannot, in consequence,
show a written document of a hundred years' existence.
``What development,'' he adds, ``can the civilization of a
people assume, if there be nothing to connect the present
with the past---if the depositories of human knowledge must
be constantly renewed---if the monuments of genius and
wisdom cannot be transmitted to posterity?''\footnote{Humboldt, 
Voyage, lib. vii., ch. 20.} Again, there
are beetles which deposit their larvae in trees in such formidable
numbers that whole forests perish beyond the power
of remedy. The pines of the Hartz have thus been destroyed
to an enormous extent; and in North America, at
one place in South Carolina, at least ninety trees in every
hundred, upon a tract of two thousand acres, were swept
away by a small black, winged bug. And yet, according
to Wilson, the historian of American birds, the people of
the United States were in the habit of destroying the redheaded
woodpecker, the great enemy of these insects,
because he occasionally spoilt an apple.\footnote{Amer. 
Ornith., i., p. 144.} The same delightful
writer and true naturalist, speaking of the labours of
the ivory-billed woodpecker, says, ``Would it be believed
that the larvae of an insect or fly, no larger than a grain
of rice, should silently, and in one season, destroy some
thousand acres of pine-trees, many of them from two to
three feet in diameter, and a hundred and fifty feet high?
In some places the whole woods, as far as you can see
around you, are dead, stripped of the bark, their wintry-looking
arms and bare trunks bleaching in the sun, and
tumbling in ruins before every blast.''\footnote{Amer. 
Ornith., iii., p. 21.} The subterraneous
larva of some species of beetle has often caused a complete
failure of the seed-corn, as in the district of Halle in
1812.\footnote{Blumenbach; see also Insect Transformations, p. 231.}
The corn-weevil, which extracts the flour from
grain, leaving the husk behind, will destroy the contents
of the largest storehouses in a very short period. The
wire-worm and the turnip-fly are dreaded by every farmer.
The ravages of the locust are too well known not to be at
once recollected as an example of the formidable collective
power of the insect race. The white ants of tropical
countries sweep away whole villages with as much certainty
as a fire or an inundation; and ships even have been
destroyed by these indefatigable republics. Our own docks
and embankments have been threatened by such minute
ravagers.

The enormous injuries which insects cause to man may
thus be held as one reason for ceasing to consider the study
of them as an insignificant pursuit; for a knowledge of their
structure, their food, their enemies, and their general habits,
may lead, as it often has led, to the means of guarding
against their injuries. At the same time we derive from
them both direct and indirect benefits. The honey of the
bee, the dye of the cochineal, and the web of the silk-worm,
the advantages of which are obvious, may well be balanced
against the destructive propensities of insects which are
offensive to man. But a philosophical study of natural
history will teach us that the direct benefits which insects
confer upon us are even less important than their general
uses in maintaining the economy of the world. The mischiefs
which result to us from the rapid increase and the
activity of insects are merely results of the very principle by
which they confer upon us numberless indirect advantages.
Forests are swept away by minute beetles; but the same
agencies relieve us from that extreme abundance of vegetable
matter which would render the earth uninhabitable were
this excess not periodically destroyed. In hot countries the
great business of removing corrupt animal matter, which the
vulture and hyaena imperfectly perform, is effected with
certainty and speed by the myriads of insects that spring
from the eggs deposited in every carcase by some fly seeking
therein the means of life for her progeny. Destruction and
reproduction, the great laws of nature, are carried on very
greatly through the instrumentality of insects; and the same
principle regulates even the increase of particular species of
insects themselves. When aphides are so abundant that we
know not how to escape their ravages, flocks of lady-birds
instantly cover our fields and gardens to destroy them.
Such considerations as these are thrown out to show that
the subject of insects has a great philosophical importance---and
what portion of the works of nature has not? The
habits of all God's creatures, whether they are noxious, or
harmless, or beneficial, are worthy objects of our study. If
they affect ourselves, in our health or our possessions, whether
for good or for evil, an additional impulse is naturally given
to our desire to attain a knowledge of their properties. Such
studies form one of the most interesting occupations which
can engage a rational and inquisitive mind; and, perhaps,
none of the employments of human life are more dignified
than the investigation and survey of the workings and the
ways of nature in the minutest of her productions.

The exercise of that habit of observation which can alone
make a naturalist---``an out-of-door naturalist,'' as Daines
Barrington calls himself---is well calculated to strengthen
even the most practical and merely useful powers of the
mind. One of the most valuable mental acquirements is
the power of discriminating among things which differ in
many minute points, but whose general similarity of appearance
usually deceives the common observer into a belief of
their identity. The study of insects, in this point of view,
is most peculiarly adapted for youth. According to our
experience, it is exceedingly difficult for persons arrived at
manhood to acquire this power of discrimination; but, in
early life, a little care on the part of the parent or teacher
will render it comparatively easy. In this study the knowledge
of things should go along with that of words. ``If
names perish,'' says Linnaeus, ``the knowledge of things
perishes also:''\footnote{Nomina si pereant, perit et cognitio rerum.}
and, without names, how can any one communicate
to another the knowledge he has acquired relative
to any particular fact, either of physiology, habit, utility, or
locality? On the other hand, mere catalogue learning
is as much to be rejected as the loose generalizations
of the despisers of classification and nomenclature.
To name a plant, or an insect, or a bird, or a quadruped
rightly, is one step towards an accurate knowledge of it;
but it is not the knowledge itself. It is the means,
and not the end in natural history, as in every other
science.

If the bias of opening curiosity be properly directed, there
is not any branch of natural history so fascinating to youth
as the study of insects. It is, indeed, a common practice in
many families to teach children, from their earliest infancy,
to treat the greater number of insects as if they were
venomous and dangerous, and, of course, meriting to be
destroyed, or at least avoided with horror. Associations
are by this means linked with the very appearance of
insects, which become gradually more inveterate with advancing
years; provided, as most frequently happens, the
same system be persisted in, of avoiding or destroying almost
every insect which is unlucky enough to attract observation.
How much rational amusement and innocent pleasure is thus
thoughtlessly lost; and how many disagreeable feelings are
thus created, in the most absurd manner! In order to show
that the study or (if the word be disliked) the observation of
insects is peculiarly fascinating to children, even in their
early infancy, we may refer to what we have seen in the
family of a friend, who is partial to this, as well as to all
the departments of natural history. Our friend's children,
a boy and girl, were taught, from the moment they could
distinguish insects, to treat them as objects of interest and
curiosity, and not to be afraid even of those which wore the
most repulsive appearance. The little girl, for example,
when just beginning to walk alone, encountered one day a
large staphylinus (\textit{Goërius olens?} \textsc{Stephens}; 
vulgo, \textit{the devil's
coach-horse}), which she fearlessly seized, and did not quit her
hold, though the insect grasped one of her fingers in his
formidable jaws. The mother, who was by, knew enough
of the insect to be rather alarmed for the consequences,
though she prudently concealed her feelings from the child.
She did well; for the insect was not strong enough to break
the skin, and the child took no notice of its attempts to bite
her finger. A whole series of disagreeable associations with
this formidable-looking family of insects was thus averted at
the very moment when a different mode of acting on the part
of the mother would have produced the contrary effect. For
more than two years after this occurrence the little girl and
her brother assisted in adding numerous specimens to their
father's collection, without the parents ever having cause,
from any accident, to repent of their employing themselves
in this manner. The sequel of the little girl's history
strikingly illustrates the position for which we contend.
The child happened to be sent to a relative in the country,
where she was not long in having carefully instilled into her
mind all the usual antipathies against ``everything that
creepeth on the earth;'' and though she afterwards returned
to her paternal home, no persuasion or remonstrance could
ever again persuade her to touch a common beetle, much less
a staphylinus, with its tail turned up in a threatening attitude,
and its formidable jaws ready extended for attack or defence.
\footnote{J. R., in Mag. of Natural History, vol. i., p. 334.}
We do not wish that children should be encouraged to expose
themselves to danger in their encounters with insects. They
should be taught to avoid those few which are really noxious---to
admire all---to injure none.

The various beauty of insects---their glittering colours,
their graceful forms---supplies an inexhaustible source of
attraction. Even the most formidable insects, both in appearance
and reality,---the dragon-fly, which is perfectly
harmless to man, and the wasp, whose sting every human
being almost instinctively shuns,---are splendid in their appearance,
and are painted with all the brilliancy of natural
hues. It has been remarked that the plumage of tropical
birds is not superior in vivid colouring to what may be
observed in the greater number of butterflies and moths.\footnote{Miss 
Jermyn's Butterfly Collector, p. 11.}
``See,'' exclaims Linnaeus, ``the large, elegant painted wings
of the butterfly, four in number, covered with delicate feathery
scales! With these it sustains itself in the air a whole day,
rivalling the flight of birds and the brilliancy of the peacock.
Consider this insect through the wonderful progress of its
life,---how different is the first period of its being from the
second, and both from the parent insect! Its changes are
an inexplicable enigma to us: we see a green caterpillar,
furnished with sixteen feet, feeding upon the leaves of a
plant; this is changed into a chrysalis, smooth, of golden
lustre, hanging suspended to a fixed point, without feet, and
subsisting without food; this insect again undergoes another
transformation, acquires wings, and six feet, and becomes a
gay butterfly, sporting in the air, and living by suction upon
the honey of plants. What has nature produced more worthy
of our admiration than such an animal, coming upon the stage
of the world, and playing its part there under so many different
masks?'' The ancients were so struck with the transformations
of the butterfly, and its revival from a seeming
temporary death, as to have considered it an emblem of the
soul, the Greek word \textit{pysche} signifying both the soul and a
butterfly; and it is for this reason that we find the butterfly
introduced into their allegorical sculptures as an emblem of
immortality. Trifling, therefore, and perhaps contemptible,
as to the unthinking may seem the study of a butterfly, yet
when we consider the art and mechanism displayed in so
minute a structure,---the fluids circulating in vessels so
small as almost to escape the sight---the beauty of the
wings and covering---and the manner in which each part is
adapted for its peculiar functions,---we cannot but be struck
with wonder and admiration, and allow, with Paley, that
``the production of beauty was as much in the Creator's
mind in painting a butterfly as in giving symmetry to the
human form.''

A collection of insects is to the true naturalist what a
collection of medals is to the accurate student of history.
The mere collector, who looks only to the shining wings
of the one, or the green rust of the other, derives little
knowledge from his pursuit. But the cabinet of the
naturalist becomes rich in the most interesting subjects of
contemplation, when he regards it in the genuine spirit of
scientific inquiry. What, for instance, can be so delightful
as to examine the wonderful variety of structure in this
portion of the creation; and, above all, to trace the beautiful
gradations by which one species runs into another?
Their differences are so minute, that an unpractised eye
would proclaim their identity; and yet, when the species
are separated, and not very distantly, they become visible
even to the common observer. It is in examinations such
as these that the naturalist finds a delight of the highest
order. While it is thus one of the legitimate objects of his
study to attend to minute differences of structure, form, and
colouring, he is not less interested in the investigation of
habits and economy; and in this respect the insect world
is inexhaustibly rich. We find herein examples of instinct
to parallel those of all the larger animals, whether they
are solitary or social; and innumerable others besides, altogether
unlike those manifested in the superior departments
of animated nature. These instincts have various directions,
and are developed in a more or less striking manner to
our senses, according to the force of the motive by which
they are governed. Some of their instincts have for their
object the preservation of insects from external attack; some
have reference to procuring food, and involve many remarkable
stratagems; some direct their social economy, and
regulate the condition under which they live together either
in monarchies or republics, their colonizations, and their
migrations; but the most powerful instinct which belongs
to insects has regard to the preservation of their species.
We find, accordingly, that as the necessity for this preservation
is of the utmost importance in the economy of nature,
so for this especial object many insects, whose offspring,
whether in the egg or the larva state, are peculiarly exposed
to danger, are endued with an almost miraculous foresight,
and with an ingenuity, perseverance, and unconquerable
industry, for the purpose of avoiding those dangers, which
are not to be paralleled even by the most singular efforts of
human contrivance. The same ingenuity which is employed
for protecting either eggs, or caterpillars and grubs, or pupae
and chrysalides, is also exercised by many insects for their
own preservation against the changes of temperature to
which they are exposed, or against their natural enemies.
Many species employ those contrivances during the period
of their hibernation, or winter sleep. For all these purposes
some dig holes in the earth, and form them into cells; others
build nests of extraneous substances, such as bits of wood
and leaves; others roll up leaves into cases, which they close
with the most curious art; others build a house of mud,
and line it with the cotton of trees, or the petals of the most
delicate flowers; others construct cells, of secretions from
their own bodies; others form cocoons, in which they
undergo their transformation; and others dig subterraneous
galleries, which, in their complexity of arrangement, in
solidity, and in complete adaptation to their purposes, vie
with the cities of civilised man. The contrivances by which
insects effect these objects have been accurately observed
and minutely described, by patient and philosophical inquirers,
who knew that such employments of the instinct
with which each species is endowed by its Creator offered
the most valuable and instructive lessons, and opened to
them a wide field of the most delightful study. The construction
of their habitations is certainly among the most
remarkable peculiarities in the economy of insects; and it is
of this subject that we propose to treat under the general
name, which is sufficiently applicable to our purpose, of
\textsc{Insect Architecture}.

In the descriptions which we shall give of Insect Architecture,
we shall employ as few technical words as possible:
and such as we cannot well avoid, we shall explain in their
places; but, since our subject chiefly relates to the reproduction
of insects, it may be useful to many readers to introduce
here a brief description of the changes which they undergo.

\begin{figure}
  \centering
  \includegraphics{images/016.png}
  \caption{Magnified eggs, of \textit{a}, 
    \textit{Geometra armillata}; 
    \textit{b}, of an unknown water insect; 
    \textit{c}, of the lacquey moth; 
    \textit{d}, of a caddis-fly (\textit{Phryganea atrata}); 
    \textit{e}, of red under-wing moth (\textit{Catocala nupta}); 
    \textit{f}, of \textit{Pontia Brassicae}; 
    \textit{g}, of the Clifden Nonpareil moth.}
\end{figure}
  
It was of old believed that insects were produced spontaneously
by putrefying substances; and Virgil gives the
details of a process for \textit{creating} a swarm of bees out of the
carcase of a bull; but Redi, a celebrated Italian naturalist,
proved by rigid experiments that they are always, in such
cases, hatched from \textit{eggs} previously laid. Most insects,
indeed, lay eggs, though some few are viviparous, and some
propagate both ways. The eggs of insects are very various
in form, and seldom shaped like those of birds. We have
here figured those of several species, as they appear under
the microscope.

When an insect first issues from the egg, it is called by
naturalists \textit{larva}, and, popularly, a caterpillar, a grub, or a
maggot. The distinction, in popular language, seems to be,
that \textit{caterpillars} are produced from the eggs of moths or
butterflies; \textit{grubs} from the eggs of beetles, bees, wasps, \&c.;
and \textit{maggots} (which are without feet) from blow-flies, house-flies,
cheese-flies, \&c., though this is not very rigidly adhered
to in common parlance. Maggots are also sometimes called
\textit{worms}, as in the instance of the meal-worm; but the common
earth-worm is not a larva, nor is it by modern naturalists
ranked among insects.

\begin{figure}
  \centering
  \includegraphics{images/017.png}
  \caption{\textit{a}, Ametabolous pupa of Cicada; 
    \textit{b}, caterpillar of tussock moth (\textit{Laria fascelina});
    \textit{c}, larva of the poplar beetle (\textit{Chrysomela populi}); 
    \textit{d}, larva of Sinex; 
    \textit{e}, larva of the common gnat.}
\end{figure}

There are, however, certain larvae, as those of the Cicada,
the crickets, the water-boatman (\textit{Notonecta}), the cockroach,
\&c., which resemble the perfect insects in form,
excepting that they are destitute of wings; but in the pupa
state these appear in a rudimentary condition, at least in
such species as have wings in the mature stage of existence.
The pupae are active and eat. Insects, the larvae and pupae
of which are so similar to the adults, are termed \textit{Ametabolous}
(\textit{a}, without, [Greek: metabolê], change); those the larvae of which
undergo changes of a marked character, \textit{Metabolous} (\textit{Insecta
ametabola} and \textit{Insecta metabola}, Burmeister).

Larvae are remarkably small at first, but grow rapidly.
The full-grown caterpillar of the goat-moth (\textit{Cossus ligniperda})
is thus seventy-two thousand times heavier than when
it issues from the egg; and the maggot of the blow-fly is, in
twenty-four hours, one hundred and fifty-five times heavier
than at its birth. Some larvae have feet, others are without;
none have wings. They cannot propagate. They feed
voraciously on coarse substances; and as they increase in
size, which they do very rapidly, they cast their skins three
or four times. In defending themselves from injury, and in
preparing for their change by the construction of secure
abodes, they manifest great ingenuity and mechanical skill.
The figures on the preceding page exemplify various forms
of insects in this stage of their existence.

\begin{figure}
  \centering
  \includegraphics{images/018.png}
  \caption{\textit{a}, Pupa of a Water-Beetle (\textit{Hydrophilus}); 
    \textit{b}, pupa of \textit{Sphinx Ligustri}.}
\end{figure}

When larvae are full grown, they cast their skins for the
last time, undergo a complete change of form, excepting in
the case of ametabolous larvae, cease to eat, and remain nearly
motionless. The inner skin of the larva now becomes converted
into a membranous or leathery covering, which wraps
the insect closely up like a mummy: in this condition it is
termed \textit{Pupa}, from its resemblance to an infant in swaddling
bands. Nympha, or nymph, is another term given to insects
in this stage;\footnote{Generally to ametabolous pupae.}
moreover from the pupae of many of the
butterflies appearing gilt as if with gold, the Greeks called
them \textit{Chrysalides}, and the Romans \textit{Aureliae}, and hence
naturalists frequently call a pupa \textit{chrysalis}, even when it is
not gilt. We shall see, as we proceed, the curious contrivances
resorted to for protecting insects in this helpless
state. The following are examples of insects in the \textit{imago},
or perfect state.

\begin{figure}
  \centering
  \includegraphics{images/019.png}
  \caption{Insects in the Imago or perfect state.
    \textit{a}, \textit{Nemopteryx coa}, \textsc{Leach}.---
    \textit{b}, \textit{Myrmeleon formicalynx}, \textsc{Fabricius}.---
    \textit{c}, \textit{Hesperia comma}, \textsc{Fabricius}.---
    \textit{d}, \textit{Nepa cinerea}, \textsc{Linnaeus}.}
\end{figure}


After a certain time, the insect which has remained in its
pupa-case, like a mass of jelly without shape, is gradually
preparing for its final change, when it takes the form of a
perfect insect. This state was called by Linnaeus \textit{Imago},
because the insect, having thrown off its mask, becomes a
perfect \textit{image} of its species. Of some, this last portion of
their existence is very short, others live through a year, and
some exist for longer periods. They feed lightly, and never
increase in size. The chief object of all is to perpetuate
their species, after which the greater number quickly die.
It is in this state that they exercise those remarkable instincts
for the preservation of their race, which are exhibited in their
preparations for the shelter of their eggs, and the nourishment
of their larvae.
